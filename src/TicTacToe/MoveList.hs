{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections #-}
module TicTacToe.MoveList
  ( empty
  , board
  , Position
  , Move
  , Board
  , BoardState
  ) where

import Data.Foldable (toList)
import Data.Maybe (isJust, fromJust)
import Data.Hashable (Hashable)
import Data.Set (Set)
import qualified Data.Set as S
import GHC.Generics (Generic)

import Cacheable (Cacheable(key))
import Dual (Dual(dual))
import Game (Game(moves, makeMove, result))
import Grid (Coordinate, Grid, coordinates, rows, cols, diag1, diag2)
import IsTree (IsTree(children))
import Valuable (Valuable(value))
import Sized (Sized(size))
import TicTacToe
  ( Conclusion(Win, Tie, Loss)
  , Player(X, O)
  )

instance Cacheable BoardState Board where
  key (BS (b, _, _)) = b

newtype BoardState = BS (Board, Player, Grid)
  deriving Show

board :: BoardState -> Board
board (BS (b, _, _)) = b

instance Sized BoardState where
  size (BS (_, _, s)) = fromIntegral s

type Position = Coordinate

type Board = [Move]

type Move = (Coordinate, Player)

instance Hashable Coordinate

data Point = P Player | Origin
  deriving (Eq, Ord, Generic)

instance Hashable Point

instance IsTree BoardState where
  children s = fromJust . (`makeMove` s) <$> moves s

instance Game BoardState Move Conclusion where
  makeMove m@(i, _) (BS (b, p, s)) = if taken i b
    then Nothing
    else Just (BS (m : b, dual p, s))
  moves bs@(BS (b, p, s)) = if isJust (result bs)
    then []
    else (, p) <$> toList (positions s `S.difference` S.fromList (fst <$> b))
  result (BS (b, p, s))
    | wins X b s = Just $ if p == X then Win else Loss
    | wins O b s = Just $ if p == O then Win else Loss
    | catsGame b s = Just Tie
    | otherwise = Nothing

taken :: Coordinate -> Board -> Bool
taken p b = p `elem` (fst <$> b)

positions :: Grid -> Set Coordinate
positions = S.fromList . coordinates

wins :: Player -> Board -> Grid -> Bool
wins p b n = fromIntegral (length positions') > n
  && any (`S.isSubsetOf` S.fromList positions') winningCoordinates
  where
    positions' = fst <$> filter ((== p) . snd) b
    winningCoordinates :: [Set Coordinate]
    winningCoordinates = S.fromList <$> lanes (isqrt n)
    lanes :: Integral a => a -> [[Coordinate]]
    lanes dim = concat $ [rows, cols, diag1, diag2] <*> [fromIntegral dim]

catsGame :: Board -> Grid -> Bool
catsGame b i = (== fromIntegral (i * i)) (length b)

instance Valuable BoardState Conclusion where
  value = fromJust . result

empty :: Grid -> BoardState
empty s = BS ([], X, s)

isqrt :: (Integral a, Integral b) => a -> b
isqrt = floor . sqrt . fromIntegral

