module Dual (Dual(dual)) where

class Dual a where
  dual :: a -> a

