{-# LANGUAGE FlexibleContexts #-}
module Main where

import Control.Monad (void)
import Control.Monad.State.Lazy
import Data.Foldable (toList)
import Data.List (intercalate)
import Data.List.Split (chunksOf)
import Data.Maybe (fromJust)
import Text.Read (readMaybe)

import Game (Game(makeMove, result), bestMoveCachedSymmetries)
import Sized (Sized(size))
import TicTacToe.PlayerArray
  ( BoardState
  , Board
  , Position
  , Move
  , Spot
  , board
  , player
  , empty
  , mkPosition
  , Conclusion
  , Player(X)
  )

main :: IO ()
main = evalStateT playGame (empty 9)

playGame :: (MonadIO m, MonadState BoardState m, MonadPlus m) => m ()
playGame = do
  liftIO $ putStrLn "Let's Play Tic Tac Toe!"
  c <- gameLoop tryMove aiMove
  liftIO $ putStrLn $ "Game over: " ++ show c

aiMove :: (MonadIO m, MonadState BoardState m, MonadPlus m) => m ()
aiMove = do
  printState
  s <- get
  put $ fromJust $ makeMove (bestMoveCachedSymmetries s) s

gameLoop :: (MonadIO m, MonadState BoardState m, MonadPlus m) =>
  m () -> m () -> m Conclusion
gameLoop p1 p2 = gets result >>= maybe (p1 >> gameLoop p2 p1) return

tryMove :: (MonadIO m, MonadState BoardState m, MonadPlus m) => m ()
tryMove = do
  printState
  m <- askMove
  s <- get
  maybe tryAgain (void . put) (makeMove m s)
  where
    tryAgain :: (MonadIO m, MonadState BoardState m, MonadPlus m) => m ()
    tryAgain = do
      liftIO $ putStrLn "Illegal move!"
      tryMove

askMove :: (MonadIO m, MonadState BoardState m) => m Move
askMove = do
  n <- gets size
  liftIO $ putStrLn $ "Choose a position (1-" ++ show n ++ "):\n"
  response <- liftIO getLine
  case parsePosition n response of
    Just p -> return (p, X)
    Nothing -> do
      liftIO $ putStrLn "invalid move"
      askMove
  where
    parsePosition :: Int -> String -> Maybe Position
    parsePosition n = (fmap (+ (-1)) . readMaybe) >=> mkPosition n

printState :: (MonadIO m, MonadState BoardState m) => m ()
printState = do
  boardState <- get
  liftIO $ putStrLn $ "Player " ++ show (player boardState) ++ "'s Turn:"
  liftIO $ putStrLn $ renderBoard (board boardState)

renderBoard :: Board -> String
renderBoard b = intercalate "\n"
  $ fmap (intercalate " | ")
  $ chunksOf (isqrt (size b))
  $ renderSpot
  <$> toList b

renderSpot :: Spot -> String
renderSpot (Just p) = show p
renderSpot Nothing = "_"

isqrt :: Int -> Int
isqrt = floor . sqrt . fromIntegral

