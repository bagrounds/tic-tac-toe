{-# LANGUAGE FunctionalDependencies #-}
module Cacheable (Cacheable(key)) where

import Data.Hashable (Hashable)

class (Hashable k, Eq k) => Cacheable s k | s -> k where
  key :: s -> k

