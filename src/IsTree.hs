module IsTree (IsTree(children)) where

class IsTree t where
  children :: t -> [t]

