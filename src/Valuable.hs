{-# LANGUAGE FunctionalDependencies #-}
module Valuable (Valuable(value)) where

import Dual (Dual)

class (Dual r, Ord r) => Valuable v r | v -> r where
  value :: v -> r

