{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
module MiniMax where

import Control.Monad.State.Lazy (State, modify, gets, evalState)
import Data.Foldable (find)
import Data.Functor ((<&>))
import Data.Functor.Foldable (Recursive(project), Corecursive, Base, hylo, ana, elgot)
import Data.HashMap.Lazy (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.Maybe (fromMaybe, catMaybes, listToMaybe)
import Data.Tree (Tree(Node))
import GHC.Generics (Generic)

import Cacheable (Cacheable(key))
import Dual (Dual(dual))
import IsTree (IsTree(children))
import Symmetrical (Symmetrical(symmetries))
import Valuable (Valuable(value))

type Algebra a b = Base (Tree a) b -> b
type Coalgebra a b = a -> Base (Tree a) b
type ElgotCoalgebra a b = a -> Either b (Base (Tree a) a)

type Caching k r = State (HashMap k r) r

type Minimaxable s r = (IsTree s, Valuable s r)
type MinimaxableAB s r = (Minimaxable s r, Bounded r)

-- naive minimax
minimax :: Minimaxable s r => s -> r
minimax = hylo findBest buildTree

-- minimax with alpha-beta pruning (general recursive)
minimaxAB :: forall s r. MinimaxableAB s r => s -> r
minimaxAB = innerMinimaxAB minBound maxBound . allOutcomes
  where
    allOutcomes :: s -> Tree r
    allOutcomes = fmap value . ana buildTree
    innerMinimaxAB :: r -> r -> Tree r -> r
    innerMinimaxAB l u (Node n []) = l `max` n `min` u
    innerMinimaxAB l u (Node _ ns) = cmx l ns
      where
        cmx :: r -> [Tree r] -> r
        cmx n [] = n
        cmx n (t:ts) = if a' >= u then a' else cmx a' ts
          where a' = dual (innerMinimaxAB (dual u) (dual n) t)

-- minimax with alpha-beta pruning (hylomorphism + inner recursion)
minimaxAB' :: MinimaxableAB s r => s -> r
minimaxAB' = hylo findBestAB buildTree

-- minimax with alpha-beta pruning (hylomorphism)
minimaxAB'' :: MinimaxableAB s r => s -> r
minimaxAB'' = hylo findBestAB' buildTree

minimaxCached :: (Minimaxable s r, Cacheable s k) => s -> r
minimaxCached = evalHashState . hylo (memoize findBest) buildTree

minimaxCachedSymmetries :: (Minimaxable s r, Cacheable s k, Symmetrical s)
  => s -> r
minimaxCachedSymmetries = evalHashState
  . hylo (memoizeSymmetries findBest) buildTree

minimaxABCached :: (MinimaxableAB s r, Cacheable s k) => s -> r
minimaxABCached = evalHashState . hylo (memoize findBestAB) buildTree

minimaxABCachedSymmetries :: (MinimaxableAB s r, Cacheable s k, Symmetrical s)
  => s -> r
minimaxABCachedSymmetries = evalHashState
  . hylo (memoizeSymmetries findBestAB) buildTree

evalHashState :: State (HashMap k v) c -> c
evalHashState = (`evalState` HashMap.empty)

-- elgot versions

-- elgot :: Functor F => (F r -> r) -> (s -> Either r (F s)) -> s -> r
minimaxElgot :: Minimaxable s r => s -> r
minimaxElgot = elgot findBest resultOrTree

-- minimax with alpha-beta pruning (elgot algebra)
-- elgot :: Functor F => (F r -> r) -> (s -> Either r (F s)) -> s -> r
-- coelgot :: Functor f => ((s, f r) -> r) -> (s -> f s) -> s -> r
minimaxABElgot :: MinimaxableAB s r => s -> r
minimaxABElgot = elgot findBestAB' resultOrTree

-- shared Algebras

findBest :: Valuable s r => Algebra s r
findBest (NodeF n []) = value n
findBest (NodeF _ ns) = dual (minimum ns)

memoizeSymmetries :: forall s r k. (Valuable s r, Cacheable s k, Symmetrical s)
  => Algebra s r -> Algebra s (Caching k r)
memoizeSymmetries alg (NodeF n fsa) =
  gets (\cache -> (`HashMap.lookup` cache) <$> keys)
  <&> (listToMaybe . catMaybes)
  >>= maybe compute pure
    where
      compute :: Caching k r
      compute = do
        fa <- sequenceA fsa
        let a = alg (NodeF n fa)
        modify (HashMap.insert (head keys) a)
        pure a
      keys = key <$> symmetries n

memoize :: forall s r k. (Valuable s r, Cacheable s k)
  => Algebra s r -> Algebra s (Caching k r)
memoize alg (NodeF n fsa) = gets (HashMap.lookup key') >>= maybe compute pure
  where
    compute :: Caching k r
    compute = do
      fa <- sequenceA fsa
      let a = alg (NodeF n fa)
      modify (HashMap.insert key' a)
      pure a
    key' = key n

findBestAB :: forall s r. (Valuable s r, Bounded r) => Algebra s r
findBestAB = findBest' minBound maxBound
  where
    findBest' :: r -> r -> Algebra s r
    findBest' l u (NodeF n []) = l `max` value n `min` u
    findBest' l u (NodeF _ ns) = cmx l ns
      where
        -- calling a recursive function from within a
        -- recursion-scheme seems self-defeating...
        cmx :: r -> [r] -> r
        cmx n [] = n
        cmx n (t:ts) = if a' >= u then a' else cmx a' ts
          where a' = dual (dual u `max` t `min` dual n)

findBestAB' :: forall s r. MinimaxableAB s r => Algebra s r
findBestAB' = findBestInner' minBound maxBound
  where
    findBestInner' :: r -> r -> Algebra s r
    findBestInner' l u (NodeF n []) = l `max` value n `min` u
    findBestInner' l u (NodeF _ ns) = cmx l ns
      where
        cmx :: r -> [r] -> r
        cmx n ts = fromMaybe (maximum zs) (find (>= u) zs)
          where
            zs = f <$> ts
            f t = dual (dual u `max` t `min` dual n)

-- Shared Coalgebras

buildTree :: IsTree s => Coalgebra s s
buildTree s = NodeF s (children s)

resultOrTree :: Minimaxable s r => ElgotCoalgebra s r
resultOrTree s = case children s of
  [] -> Left (value s)
  ss -> Right (NodeF s ss)

-- define a Base functor for Tree

data TreeF a b = NodeF a [b]
  deriving (Show, Eq, Functor, Generic)

instance Corecursive (Tree a)

instance Recursive (Tree a) where
  project (Node n ns) = NodeF n ns

type instance Base (Tree a) = TreeF a

