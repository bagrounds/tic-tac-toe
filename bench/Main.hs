import Criterion.Main (defaultMainWith, defaultConfig)
import Criterion.Types (reportFile, nf, bench, bgroup)

import MiniMax
import qualified TicTacToe.PlayerArray as PA
import qualified TicTacToe.BitVector as BV
--import qualified TicTacToe.MoveList as ML
--import qualified TicTacToe.MoveListNF as MLNF

main :: IO ()
main = defaultMainWith config
  [ bgroup "BitVector"
    $ mkBench (BV.empty 9) <$>
    [ ("minimax", minimax)
    , ("minimaxCached", minimaxCached)
    , ("minimaxAB", minimaxAB)
    , ("minimaxAB'", minimaxAB')
    , ("minimaxAB''", minimaxAB'')
    , ("minimaxABElgot", minimaxABElgot)
    , ("minimaxABCached", minimaxABCached)
    ]
-- I seem to have broken these with careless refactoring
{-  , bgroup "MoveList"
    $ mkBench (ML.empty 3) <$>
    [ ("minimax", minimax)
    , ("minimaxCached", minimaxCached)
    , ("minimaxAB", minimaxAB)
    , ("minimaxAB'", minimaxAB')
    , ("minimaxAB''", minimaxAB'')
    , ("minimaxABElgot", minimaxABElgot)
    , ("minimaxABCached", minimaxABCached)
    ]
  , bgroup "MoveListNF"
    $ mkBench (MLNF.empty 3) <$>
    [ ("minimaxCached", minimaxCached)
    , ("minimaxABCached", minimaxABCached)
    ]-}
  , bgroup "PlayerArray"
    $ mkBench (PA.empty 9) <$>
    [ ("minimax", minimax)
    , ("minimaxElgot", minimaxElgot)
    , ("minimaxAB", minimaxAB)
    , ("minimaxAB'", minimaxAB')
    , ("minimaxAB''", minimaxAB'')
    , ("minimaxABElgot", minimaxABElgot)
    , ("minimaxCached", minimaxCached)
    , ("minimaxABCached", minimaxABCached)
    , ("minimaxCachedSymmetries", minimaxCachedSymmetries)
    , ("minimaxABCachedSymmetries", minimaxABCachedSymmetries)
    ]
  ]
  where
    config = defaultConfig { reportFile = Just "bench.html" }
    mkBench board (n, f) = bench n (nf f board)

