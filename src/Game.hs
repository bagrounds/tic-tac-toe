{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Game
  ( Game(makeMove, moves, result)
  , bestMove
  , bestMoveCachedSymmetries
  ) where

import Data.Foldable (minimumBy)
import Data.Function (on)

import Cacheable (Cacheable)
import MiniMax (MinimaxableAB, minimaxAB, minimaxABCachedSymmetries)
import Symmetrical (Symmetrical)

class Game state move result | state -> move result where
  makeMove :: move -> state -> Maybe state
  moves :: state -> [move]
  result :: state -> Maybe result

bestMove :: (MinimaxableAB s r, Game s m r) => s -> m
bestMove = bestMoveWith minimaxAB

bestMoveCachedSymmetries ::
  (MinimaxableAB s r, Cacheable s k, Symmetrical s, Game s m r) => s -> m
bestMoveCachedSymmetries = bestMoveWith minimaxABCachedSymmetries

bestMoveWith :: forall s m r. (Ord r, Game s m r) => (s -> r) -> s -> m
bestMoveWith bestScore s =
  fst . minimumBy (compare `on` snd) $ (`eval` s) <$> moves s
    where
      eval :: m -> s -> (m, Maybe r)
      eval m s' = (m, bestScore <$> makeMove m s')

