module Symmetrical (Symmetrical(symmetries)) where

class Symmetrical s where
  symmetries :: s -> [s]

