{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections #-}
module TicTacToe.BitVector
  ( BoardState
  , empty
  ) where

import Data.BitVector (BV, (.|.), (.&.), (@.))
import qualified Data.BitVector as BV
import Data.Maybe (isJust, fromJust)

import Cacheable (Cacheable(key))
import Game (Game(moves, makeMove, result))
import Grid (Coordinate, rows, cols, diag1, diag2, coordinateIndex)
import IsTree (IsTree(children))
import TicTacToe (Player(X, O), Conclusion(Loss, Tie, Win))
import Valuable (Valuable(value))

newtype BoardState = BoardState (Board, Player)
  deriving Show

type Board = (BV, BV)
type Move = (Position, Player)
type Position = Int

empty :: Int -> BoardState
empty i = BoardState ((BV.zeros i, BV.zeros i), X)

instance Valuable BoardState Conclusion where
  value = fromJust . result

instance Cacheable BoardState (Integer, Integer) where
  key (BoardState ((xs, os), _)) = (BV.int xs, BV.int os)

instance IsTree BoardState where
  children s = fromJust . (`makeMove` s) <$> moves s

instance Game BoardState Move Conclusion where
  makeMove (i, _) (BoardState ((xs, os), X)) = if (xs .|. os) @. i
    then Nothing
    else  Just (BoardState ((update xs i, os), O))
  makeMove (i, _) (BoardState ((xs, os), O)) = if (xs .|. os) @. i
    then Nothing
    else Just (BoardState ((xs, update os i), X))
  moves bs@(BoardState ((xs, os), p)) = if isJust (result bs)
    then []
    else (, p) <$> positions
    where
      positions = snd <$> filter fst (openBits `zip` [0..])
      openBits = BV.toBits $ BV.reverse (BV.nor xs os)
  result (BoardState ((xs, os), p))
    | wins xs = Just $ if p == X then Win else Loss
    | wins os = Just $ if p == O then Win else Loss
    | catsGame xs os = Just Tie
    | otherwise = Nothing

wins :: BV -> Bool
wins bv = any (matches bv) (winningPositions (BV.size bv))

winningPositions :: Int -> [BV]
winningPositions s = positionListToBitVector <$> lanes dim
  where
    dim = isqrt s - 1
    positionListToBitVector :: [Coordinate] -> BV
    positionListToBitVector pl =
      BV.or $ single s . coordinateIndex (dim + 1) <$> pl

lanes :: Integral a => a -> [[Coordinate]]
lanes dim = concat $ [rows, cols, diag1, diag2] <*> [fromIntegral dim]

matches :: BV -> BV -> Bool
matches board mask = board .&. mask == mask

catsGame :: BV -> BV -> Bool
catsGame xs os = (xs .|. os) == BV.ones (BV.size xs)

update :: BV -> Position -> BV
update b i = b .|. single (BV.size b) i

single :: Int -> Position -> BV
single s pos = BV.bitVec s (2 ^ pos)

isqrt :: (Integral a, Integral b) => a -> b
isqrt = floor . sqrt . fromIntegral

