{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
module TicTacToe.PlayerArray
  ( BoardState
  , Board
  , Position
  , Move
  , Spot
  , board
  , player
  , empty
  , mkPosition
  , Conclusion
  , Player(X, O)
  ) where

import Data.Coerce (coerce)
import Data.Array (Array, Ix, assocs, listArray, (!), (//))
import Data.Array.Base (numElements)
import Data.Foldable (find, all, toList)
import Data.List (nub)
import qualified Data.Matrix as M
import Data.Maybe (isNothing, isJust, fromJust)
import Data.Hashable (Hashable(..), hashUsing)

import TicTacToe
  ( Conclusion(Loss, Tie, Win)
  , Player(X, O)
  )
import Cacheable (Cacheable(key))
import Dual (Dual(dual))
import IsTree (IsTree(children))
import Valuable (Valuable(value))
import Game (Game(makeMove, moves, result))
import Sized (Sized(size))
import Symmetrical (Symmetrical(symmetries))

newtype Position = Position Int
  deriving (Eq, Ord, Enum, Ix)

instance Show Position where
  show (Position i) = show i

type Spot = Maybe Player

type Board = Array Position Spot

instance Sized Board where
  size = numElements

instance Hashable Board where
  hashWithSalt = hashUsing toList

newtype BoardState = BoardState (Board, Player)
  deriving Show

board :: BoardState -> Board
board (BoardState (b, _)) = b

player :: BoardState -> Player
player (BoardState (_, p)) = p

instance Sized BoardState where
  size (BoardState (b, _)) = size b

instance IsTree BoardState where
  children s = fromJust . (`makeMove` s) <$> moves s

instance Valuable BoardState Conclusion where
  value (BoardState (b, p)) = case find allEqualAndJust (lanes b) of
    Just [] -> error "found empty list in result"
    Just (p' : _) -> if p' == Just p then Win else Loss
    Nothing -> if full b
      then Tie
      else error "this game is not over!"

type Move = (Position, Player)

empty :: Int -> BoardState
empty i = coerce (emptyBoard i, X)

emptyBoard :: Int -> Board
emptyBoard i = listArray (Position 0, Position (i - 1)) (replicate i Nothing)

(///) :: Ix i => Array i e -> (i, e) -> Array i e
a /// u = a // [u]

instance Game BoardState Move Conclusion where
  makeMove (i, _) (BoardState (b, p)) = if isJust (b ! i)
    then Nothing
    else coerce $ Just (b /// (i, Just p), dual p)
  moves bs@(BoardState (b, p)) = if isJust (result bs)
    then []
    else (,p) . fst <$> filter (isNothing . snd) (assocs b)
  result (BoardState (b, p)) = case find allEqualAndJust (lanes b) of
    Just [] -> error "found empty list in result"
    Just (p' : _) -> Just (if p' == Just p then Win else Loss)
    Nothing -> if full b then Just Tie else Nothing

full :: Array Position (Maybe a) -> Bool
full = not . any isNothing 

allEqualAndJust :: (Eq a) => [Maybe a] -> Bool
allEqualAndJust = pAnd ((== 1) . length . nub) (all isJust)

pAnd :: (a -> Bool) -> (a -> Bool) -> a -> Bool
pAnd f g x = f x && g x

lanes :: Array Position a -> [[a]]
lanes s = rows s ++ columns s ++ diags s

rows :: Array Position a -> [[a]]
rows v = [toList (M.getRow row m) | row <- [1..n]]
  where
    n = isqrt (numElements v)
    m = M.fromList n n (toList v)

columns :: Array Position a -> [[a]]
columns v = [toList (M.getCol col m) | col <- [1..n]]
  where
    n = isqrt (numElements v)
    m = M.fromList n n (toList v)

diags :: Array Position a -> [[a]]
diags v = toList <$> [M.getDiag m, M.getDiag (flipMatrix m)]
  where
    n = isqrt (numElements v)
    m = M.fromList n n (toList v)

flipMatrix :: M.Matrix a -> M.Matrix a
flipMatrix mat = flipMat mat
  where
    n = M.nrows mat
    m = n `div` 2
    flipMat = foldr (.) id [M.switchRows i (n - i + 1) | i <- [1..m]]

mkPosition :: Int -> Int -> Maybe Position
mkPosition size' i
  | i >= 0 && i < size' = Just (Position i)
  | otherwise = Nothing

isqrt :: Int -> Int
isqrt = floor . sqrt . fromIntegral

instance Cacheable BoardState Board where
  key (BoardState (b, _)) = b

instance Symmetrical BoardState where
  symmetries (BoardState (b, p)) = coerce $ (, p) <$> ((b :)
    $ asMatrix <$>
    [ M.transpose
    , flipMatrix
    , M.transpose . flipMatrix
    , flipMatrix . M.transpose
    , flipMatrix . M.transpose . flipMatrix
    , M.transpose . flipMatrix . M.transpose
    , flipMatrix . M.transpose . flipMatrix . M.transpose
    ] <*> [b])
    where
      asMatrix :: Foldable f =>
        (M.Matrix a -> M.Matrix b) -> f a -> Array Position b
      asMatrix f = matrixToArray . f . M.fromList 3 3 . toList
      matrixToArray :: M.Matrix a -> Array Position a
      matrixToArray m = listArray (Position 0, Position (n * n - 1)) (concat rows')
        where
          n = M.nrows m
          rows' = toList . flip M.getRow m <$> [1..n]

