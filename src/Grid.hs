{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Grid 
  ( Coordinate
  , Grid
  , coordinates
  , centerCoordinates
  , distanceSquared
  , rows
  , cols 
  , diag1
  , diag2
  , coordinateIndex
  ) where

import Data.Coerce (Coercible, coerce)
import Data.Ix (Ix, index, range)
import GHC.Generics (Generic)
import Numeric.Natural (Natural)

newtype Grid = Grid Natural
  deriving (Eq, Show, Ord, Ix, Real, Num, Enum, Integral)

newtype Coordinate = Coordinate (Natural, Natural)
  deriving (Eq, Show, Ord, Ix, Generic)

rows :: Grid -> [[Coordinate]]
rows n = coerce [[(i, j) | i <- [0..n]] | j <- [0..n]]

cols :: Grid -> [[Coordinate]]
cols n = coerce [[(j, i) | i <- [0..n]] | j <- [0..n]]

diag1 :: Grid -> [[Coordinate]]
diag1 n = coerce [[(i, i) | i <- [0..n]]]

diag2 :: Grid -> [[Coordinate]]
diag2 n = coerce [[(i, n - i) | i <- [0..n]]]

coordinates :: (Num a, Ix a, Coercible Grid a) => a -> [Coordinate]
coordinates n = coerce $ range ((0, 0), (n, n))

coordinateIndex :: (Num a, Ix a) => Grid -> Coordinate -> a
coordinateIndex (Grid i) =
  fromIntegral . index (Coordinate (0, 0), Coordinate (i, i))

distanceSquared :: Coordinate -> Coordinate -> Natural
distanceSquared (Coordinate (x1, y1)) (Coordinate (x2, y2)) = dx ^ 2 + dy ^ 2
  where
    dx = dist x1 x2
    dy = dist y1 y2
    dist a b = abs (a - b)

centerCoordinates :: Grid -> [Coordinate]
centerCoordinates n
  | even n = coerce [(c1, c1)]
  | otherwise = coerce [(i, j) | i <- cs, j <- cs]
  where
    cs = [c1, c2]
    c1 = n `div` 2
    c2 = n `div` 2 + 1

