# tic-tac-toe

Work in progress

* [Haddock][5]
* [Benchmarks][6]
* [Test Coverage][7]

## Development

```bash
stack build --profile
```

```bash
stack build --profile --file-watch
```

```bash
stack test --profile
```

```bash
stack ghci --load-local-deps
```

```bash
stack run --profile -- +RTS -p # generates tic-tac-toe.prof
```

## References
* A Google search pointed me to [this][1] Haskell implementation of minimax,
which inspired some code found here.
* The above is based on Bird & Wadler "Introduction to Functional Programming",
a copy of which can be found [here][2]
* A great [blog][3] series introducing recursion schemes
* This [gist][4] helped me figure out how to add caching to recursion schemes

[1]: https://hackage.haskell.org/package/lostcities-0.2/src/Minimax.hs
[2]: https://usi-pl.github.io/lc/sp-2015/doc/Bird_Wadler.%20Introduction%20to%20Functional%20Programming.1ed.pdf
[3]: https://blog.sumtypeofway.com/an-introduction-to-recursion-schemes/
[4]: https://gist.github.com/gelisam/64674fd783584ef8eeab7a11bb72c2a5
[5]: https://bagrounds.gitlab.io/tic-tac-toe/
[6]: https://bagrounds.gitlab.io/tic-tac-toe/bench
[7]: https://bagrounds.gitlab.io/tic-tac-toe/coverage

