{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TupleSections #-}
module TicTacToe.MoveListNF
  ( empty
  , BoardState
  ) where

import Data.Coerce (coerce)
import Data.Foldable (toList)
import Data.Maybe (fromJust)
import Data.Hashable (Hashable(..), hashUsing)
import Data.Set (Set)
import qualified Data.Set as S
import GHC.Generics (Generic)
import Numeric.Natural (Natural)

import Cacheable (Cacheable(key))
import Game (Game(moves, makeMove, result))
import Grid (Grid, distanceSquared, centerCoordinates)
import IsTree (IsTree)
import Valuable (Valuable(value))
import Sized (Sized(size))
import TicTacToe (Conclusion, Player)
import TicTacToe.MoveList (Position, Move)
import qualified TicTacToe.MoveList as ML

newtype BoardState = BoardState ML.BoardState
  deriving (IsTree, Sized, Show)

instance Game BoardState Move Conclusion where
  makeMove m b = makeMove m (coerce b)
  moves b = moves (coerce b :: ML.BoardState)
  result b = result (coerce b :: ML.BoardState)

instance Valuable BoardState Conclusion where
  value = fromJust . result

instance Cacheable BoardState NormalForm where
  key = normalForm

empty :: Grid -> BoardState
empty = BoardState . ML.empty

type Location = (Position, Point)
type Locations = [Location]
type Edges = Set Edge
type NormalForm = Set Edges

data Edge = Edge Point Natural
  deriving (Eq, Ord, Generic)

instance Hashable Edge
instance Hashable a => Hashable (Set a) where
  hashWithSalt = hashUsing toList

data Point = P Player | Origin
  deriving (Eq, Ord, Generic)

instance Hashable Point

normalForm :: BoardState -> Set Edges
normalForm (BoardState bs) = S.fromList
  $ edges <$> ML.board bs <*> pure (origins (fromIntegral $ size bs) ++ b')
  where
    b' = toLocation <$> ML.board bs

origins :: Grid -> [Location]
origins n = (, Origin) <$> centerCoordinates n

edges :: Move -> Locations -> Edges
edges me b = S.fromList $ edge (toLocation me) <$> others
  where
    others = filter (/= toLocation me) b

edge :: Location -> Location -> Edge
edge (pos1, _) (pos2, p2) = Edge p2 (distanceSquared pos1 pos2)

toLocation :: Move -> Location
toLocation (pos, p) = (pos, P p)

