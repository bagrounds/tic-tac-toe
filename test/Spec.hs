{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad.Extra (liftMaybe)
import Control.Monad.State.Lazy
import Control.Monad.Trans.Maybe
import Data.Functor.Identity
import Data.Maybe (isNothing, isJust, fromJust)
import Test.QuickCheck

import Game
import IsTree (IsTree(children))
import TicTacToe (Player(X, O))
import qualified TicTacToe.PlayerArray as PA
import qualified TicTacToe.BitVector as BV
import qualified TicTacToe.MoveList as ML
import qualified TicTacToe.MoveListNF as MLNF

main :: IO ()
main = do
  quickCheck $ prop_exclusive_positions (PA.empty 9)
  quickCheck $ prop_available_moves_can_be_made @PA.BoardState
  quickCheck $ prop_available_moves_can_be_made @BV.BoardState
  quickCheck $ prop_available_moves_can_be_made @ML.BoardState
  quickCheck $ prop_result_xor_moves @PA.BoardState
  quickCheck $ prop_result_xor_moves @BV.BoardState
  quickCheck $ prop_result_xor_moves @ML.BoardState

  -- Not sure why this test hangs (infinite loop somewhere maybe?)
  --quickCheck $ prop_available_moves_can_be_made @MLNF.BoardState

instance Arbitrary PA.Position where
  arbitrary = fromJust . PA.mkPosition 9 <$> elements [0..8]

instance Arbitrary MLNF.BoardState where
  arbitrary = arbitraryNode 9 (MLNF.empty 3)

instance Arbitrary ML.BoardState where
  arbitrary = arbitraryNode 9 (ML.empty 3)

instance Arbitrary BV.BoardState where
  arbitrary = arbitraryNode 9 (BV.empty 9)

instance Arbitrary PA.BoardState where
  arbitrary = arbitraryNode 9 (PA.empty 9)

arbitraryNode :: IsTree s => Int -> s -> Gen s
arbitraryNode maxDepth root = elements [0..maxDepth] >>= descend root
  where
    descend :: IsTree s => s -> Int -> Gen s
    descend node 0 = return node
    descend node n = case children node of
      [] -> return node
      nodes -> do
        child <- elements nodes
        descend child (n - 1)

prop_exclusive_positions :: PA.BoardState -> PA.Position -> Bool
prop_exclusive_positions initialState position =
  isNothing r0
  where
    r0 :: Maybe ((), PA.BoardState)
    r0 = runIdentity $ runMaybeT $ runStateT moveSamePlaceTwice initialState
    moveSamePlaceTwice :: (MonadState PA.BoardState m, MonadPlus m) => m ()
    moveSamePlaceTwice = do
      s0 <- get
      s1 <- liftMaybe $ makeMove (position, X) s0
      _ <- liftMaybe $ makeMove (position, O) s1
      return ()

prop_available_moves_can_be_made :: Game s m r => s -> Bool
prop_available_moves_can_be_made s0 = let
  ms = moves s0
  newStates = makeMove <$> ms <*> pure s0 in
  all isJust newStates

prop_result_xor_moves :: Game s m r => s -> Bool
prop_result_xor_moves s0 = case moves s0 of
  [] -> isJust (result s0)
  _ -> isNothing (result s0)

