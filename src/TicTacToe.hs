{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
module TicTacToe
  ( Conclusion(Loss, Tie, Win)
  , Player(X, O)
  ) where

import Control.DeepSeq (NFData)
import Data.Hashable (Hashable)
import GHC.Generics (Generic)

import Dual (Dual(dual))

data Conclusion = Loss | Tie | Win
  deriving (Show, Eq, Ord, Bounded, Generic, NFData)

instance Dual Conclusion where
  dual Win = Loss
  dual Tie = Tie
  dual Loss = Win

data Player = X | O
  deriving (Show, Eq, Ord, Generic)

instance Dual Player where
  dual X = O
  dual O = X

instance Hashable Player

