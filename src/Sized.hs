module Sized (Sized(size)) where

class Sized a where
  size :: a -> Int

